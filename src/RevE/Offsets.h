#pragma once

class Offsets
{
public:
	static float* DeltaTime;
	static float* DeltaTimeRealTime;
	static float* DurationOfApplicationRunTimeMS;

	static float* HUDOpacity;

	static uintptr_t WorldToCamMatrix;
	static RE::NiRect<float>* ViewPort;

	// Raycast (from SmoothCam)
	static uintptr_t CameraCaster;
	static uintptr_t GetNiAVObject;

	typedef __int64(__fastcall RE::Actor::* DismountActor_func)() const;
	static REL::Relocation<DismountActor_func> DismountActor;

	static float GetWorldScale();
	static float GetWorldScaleInverse();
};
