set SkyrimPath=D:/Program Files (x86)/Steam/steamapps/common/Skyrim Special Edition

echo "Copying files to Skyrim game folder"

xcopy /s/e/y/v "scripts" "%SkyrimPath%\Data\scripts\"
xcopy /s/e/y/v "..\Interface" "%SkyrimPath%\Data\Interface\"

xcopy /s/e/y/v "..\MCM\config\BetterThirdPersonSelection\config.json" "%SkyrimPath%\Data\MCM\config\BetterThirdPersonSelection\"
xcopy /s/e/y/v "..\MCM\config\BetterThirdPersonSelection\settings.ini" "%SkyrimPath%\Data\MCM\config\BetterThirdPersonSelection\"
xcopy /s/e/y/v "..\MCM\config\BetterThirdPersonSelection\FilterPresets_Default.toml" "%SkyrimPath%\Data\MCM\config\BetterThirdPersonSelection\"
xcopy /s/e/y/v "..\MCM\config\BetterThirdPersonSelection\ObjectFilterList_Default.toml" "%SkyrimPath%\Data\MCM\config\BetterThirdPersonSelection\"