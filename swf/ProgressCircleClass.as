﻿import com.greensock.TimelineLite;
import com.greensock.TweenLite;
import com.greensock.easing.*;
import flash.geom.Point;
import flash.filters.BlurFilter;

class ProgressCircleClass extends MovieClip
{
	var ProgressCircleRadius;
	var ProgressCircleThickness;
	var ProgressCircleAlpha;
	var ProgressCircleColor;
	
	var ProgressCircleValue;
	
	var DropShadowThickness;
	var DropShadowOffset;
	var DropShadowColor;
	var DropShadowAlpha;
	//var DropShadowBlurAmount;
	//var DropShadowBlurQuality;
	
	var TotalCircleNumSegments;
	var TWO_PI;
	var HALF_PI;
	
	var Circle:MovieClip;
	var DropShadow:MovieClip;
	
	public function ProgressCircleClass() 
	{
		super.ProgressCircleClass();
		
		DropShadow = this.createEmptyMovieClip("DropShadow", this.getNextHighestDepth());
		Circle = this.createEmptyMovieClip("Dircle", this.getNextHighestDepth());
		
		ProgressCircleRadius = 18.0;
		ProgressCircleThickness = 5;
		ProgressCircleAlpha = 100.0;
		ProgressCircleColor = 0xAAACAE; //gray
		
		ProgressCircleValue = 1.0;
		
		DropShadowThickness = 8;
		DropShadowOffset = 4;
		DropShadowColor = 0x292929;
		DropShadowAlpha = 50;
		//DropShadowBlurAmount = 10;
		//DropShadowBlurQuality = 2;
		
		TotalCircleNumSegments = 32;
		
		TWO_PI = Math.PI * 2;
		HALF_PI = Math.PI / 2;
	}
	
	public function SetProgressCircleProgress(newProgress)
	{
		ProgressCircleValue = newProgress;
		
		DrawDropShadow();
		DrawCircle();
	}
	
	public function DrawCircle()
	{
		Circle.clear();
		Circle.lineStyle(ProgressCircleThickness, ProgressCircleColor, ProgressCircleAlpha);
		
		var startPoint : Point = GetPointOnCircle(0, 0);
		Circle.moveTo(startPoint.x, startPoint.y);
		
		for (var i = 0; i <= TotalCircleNumSegments; i++)
		{
			var currValue = i / TotalCircleNumSegments;
			var shouldBreak = currValue > ProgressCircleValue;
			
			if (shouldBreak)
				currValue = ProgressCircleValue;
			
			var currPoint : Point = GetPointOnCircle(currValue);
			
			Circle.lineTo(currPoint.x, currPoint.y);
			Circle.moveTo(currPoint.x, currPoint.y);
			
			if (shouldBreak)
				break;
		}

		Circle.endFill();
	}
	
	public function DrawDropShadow()
	{
		DropShadow.clear();
		DropShadow.lineStyle(DropShadowThickness, DropShadowColor, DropShadowAlpha);
		
		var startPoint : Point = GetPointOnCircle(0, 0);
		DropShadow.moveTo(startPoint.x, startPoint.y);
		
		for (var i = 0; i <= TotalCircleNumSegments; i++)
		{
			var currValue = i / TotalCircleNumSegments;
			var shouldBreak = currValue > ProgressCircleValue;
			
			if (shouldBreak)
				currValue = ProgressCircleValue;
			
			var currPoint : Point = GetPointOnCircle(currValue);
			currPoint.x += DropShadowOffset;
			
			DropShadow.lineTo(currPoint.x, currPoint.y);
			DropShadow.moveTo(currPoint.x, currPoint.y);
			
			if (shouldBreak)
				break;
		}
		
		// ignored by Scaleform
		//var filter:BlurFilter = new BlurFilter(DropShadowBlurAmount, DropShadowBlurAmount, DropShadowBlurQuality);
		//var filterArray:Array = new Array();
		//filterArray.push(filter);
		//DropShadow.filters = filterArray;

		DropShadow.endFill();
	}
	
	public function GetPointOnCircle(valueIn)
	{
		var pointOut : Point = new Point(0, 0);
		
		var angle = valueIn * TWO_PI;
		angle -= HALF_PI; // start above circle center
		
		pointOut.x = ProgressCircleRadius * Math.cos(angle);
		pointOut.y = ProgressCircleRadius * Math.sin(angle);
		
		return pointOut;
	}
}
