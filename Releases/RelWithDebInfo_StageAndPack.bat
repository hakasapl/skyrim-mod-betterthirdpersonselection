set version="0.5.6"

xcopy /s/e/y/v "..\Interface" "Stage\SE\Data\Interface\"
xcopy /s/e/y/v "..\MCM" "Stage\SE\Data\MCM\"
xcopy /s/e/y/v "..\scripts\scripts" "Stage\SE\Data\scripts\"
xcopy /s/e/y/v "..\scripts\source" "Stage\SE\Data\scripts\source\"

xcopy /s/e/y/v "..\Interface" "Stage\AE\Data\Interface\"
xcopy /s/e/y/v "..\MCM" "Stage\AE\Data\MCM\"
xcopy /s/e/y/v "..\scripts\scripts" "Stage\AE\Data\scripts\"
xcopy /s/e/y/v "..\scripts\source" "Stage\AE\Data\scripts\source\"

xcopy /F/y "..\BetterThirdPersonSelection.esp" "Stage\SE\Data\"
xcopy /F/y "..\BetterThirdPersonSelection.esp" "Stage\AE\Data\"

xcopy /F/y "..\build\pre-ae\RelWithDebInfo\BetterThirdPersonSelection.dll" "Stage\SE\Data\SKSE\Plugins\"
xcopy /F/y "..\build\pre-ae\RelWithDebInfo\BetterThirdPersonSelection.pdb" "PDBs\SE\BTPS_SE_%version%.pdb*"

xcopy /F/y "..\build\post-ae\RelWithDebInfo\BetterThirdPersonSelection.dll" "Stage\AE\Data\SKSE\Plugins\"
xcopy /F/y "..\build\post-ae\RelWithDebInfo\BetterThirdPersonSelection.pdb" "PDBs\AE\BTPS_AE_%version%.pdb*"

cd "Stage\SE"
"D:\Program Files\7-Zip\7z.exe" a "..\..\Rel_SE\BTPS_SE_%version%.7z" "*"

cd "..\.."

cd "Stage\AE"
"D:\Program Files\7-Zip\7z.exe" a "..\..\Rel_AE\BTPS_AE_%version%.7z" "*"

pause