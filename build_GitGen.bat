:: uses my own CLib fork
git submodule add https://gitlab.com/HammelGammel/CommonLibSSE.git ./external/CommonLibSSE
cd ./external/CommonLibSSE
git checkout dev-custom-se
cd ../../

git submodule add https://gitlab.com/HammelGammel/CommonLibSSE.git ./external/CommonLibAE
cd ./external/CommonLibAE
git checkout dev-custom-ae
cd ../../

pause